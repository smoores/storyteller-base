FROM ubuntu:22.04

RUN echo "Package: *\nPin: release o=repo.radeon.com\nPin-Priority: 600" > /etc/apt/preferences.d/rocm-pin-600
RUN apt-get update && \
  DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
  build-essential \
  git \
  cmake \
  software-properties-common \
  sqlite3 \
  libsqlite3-dev \
  argon2 \
  wget \
  ca-certificates \
  curl \
  gnupg \
  libelf1 \
  libnuma-dev \
  kmod \
  file \
  python3 \
  python3-pip \
  tini && \
  apt-get clean

COPY scripts/install-node.sh .

ARG TARGETARCH
# Manually install node 22 from prebuilt binary
RUN ./install-node.sh

COPY --link --from=mwader/static-ffmpeg:6.1.1 /ffmpeg /usr/local/bin/
COPY --link --from=mwader/static-ffmpeg:6.1.1 /ffprobe /usr/local/bin/
